package image

import (
	"github.com/google/go-containerregistry/pkg/name"
	"regexp"
	"strings"
)

type reference struct {
	id string
}

func (t *reference) String() string {
	return t.id
}

func (t *reference) Context() name.Repository {
	return name.Repository{}
}

func (t *reference) Identifier() string {
	return t.id
}

func (t *reference) Scope(string) string {
	return ""
}

func (t *reference) Name() string {
	str := strings.TrimPrefix(t.id, "sha256:")
	if len(str) > 12 {
		return str[:12]
	}

	return t.id
}

var (
	imageIdRegex      = regexp.MustCompile(`^(sha256:)?([a-f0-9]{64})$`)
	imageIdShortRegex = regexp.MustCompile(`^[a-f0-9]{12}$`)
)

func ParseReference(s string, opts ...name.Option) (name.Reference, error) {
	if imageIdRegex.MatchString(s) || imageIdShortRegex.MatchString(s) {
		return &reference{s}, nil
	}

	return name.ParseReference(s, opts...)
}
